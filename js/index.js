$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval: 4000
    });
    // Boton editar y modal nota 1
    $('#editModal').on('show.bs.modal', function (e) {
        console.log("El modal 1 comenzó a abrirse");
        $('#editBtn').removeClass('btn-editar');
        $('#editBtn').addClass('btn-success');
        $('#editBtn').prop('disabled', true);
    });
    $('#editModal').on('shown.bs.modal', function (e) {
        console.log("El modal 1 termino de abrirse");

    });
    $('#editModal').on('hide.bs.modal', function (e) {
        console.log("El modal editar 1 comenzó a ocultarse")
    });
    $('#editModal').on('hidden.bs.modal', function (e) {
        console.log("El modal editar 1 termino de cerrarse");
        $('#editBtn').prop('disabled', false);
        $('#editBtn').removeClass('btn-success');
        $('#editBtn').addClass('btn-editar');
    });
    // Boton editar y modal nota 2
    $('#editModal2').on('show.bs.modal', function (e) {
        console.log("El modal editar 2 comenzó a abrirse");
        $('#editBtn2').removeClass('btn-editar');
        $('#editBtn2').addClass('btn-success');
        $('#editBtn2').prop('disabled', true);
    });
    $('#editModal2').on('shown.bs.modal', function (e) {
        console.log("El modal editar 2 termino de abrirse");

    });
    $('#editModal2').on('hide.bs.modal', function (e) {
        console.log("El modal editar 2 comenzó a ocultarse")
    });
    $('#editModal2').on('hidden.bs.modal', function (e) {
        console.log("El modal editar 2 termino de cerrarse");
        $('#editBtn2').prop('disabled', false);
        $('#editBtn2').removeClass('btn-success');
        $('#editBtn2').addClass('btn-editar');
    });
    // Boton editar y modal nota 3
    $('#editModal3').on('show.bs.modal', function (e) {
        console.log("El modal editar 3 comenzó a abrirse");
        $('#editBtn3').removeClass('btn-editar');
        $('#editBtn3').addClass('btn-success');
        $('#editBtn3').prop('disabled', true);
    });
    $('#editModal3').on('shown.bs.modal', function (e) {
        console.log("El modal editar 3 termino de abrirse");

    });
    $('#editModal3').on('hide.bs.modal', function (e) {
        console.log("El modal editar 3 comenzó a ocultarse")
    });
    $('#editModal3').on('hidden.bs.modal', function (e) {
        console.log("El modal editar 3 termino de cerrarse");
        $('#editBtn3').prop('disabled', false);
        $('#editBtn3').removeClass('btn-success');
        $('#editBtn3').addClass('btn-editar');
    });

    // Boton borrar y modal nota 1 
    $('#deleteModal').on('show.bs.modal', function (e) {
        console.log("El modal borrar 1 comenzó a abrirse");
        $('#deleteBtn').removeClass('btn-borrar');
        $('#deleteBtn').addClass('btn-success');
        $('#deleteBtn').prop('disabled', true);
    });
    $('#deleteModal').on('shown.bs.modal', function (e) {
        console.log("El modal borrar 1 termino de abrirse");

    });
    $('#deleteModal').on('hide.bs.modal', function (e) {
        console.log("El modal borrar 1 comenzó a ocultarse")
    });
    $('#deleteModal').on('hidden.bs.modal', function (e) {
        console.log("El modal borrar 1 termino de cerrarse");
        $('#deleteBtn').prop('disabled', false);
        $('#deleteBtn').removeClass('btn-success');
        $('#deleteBtn').addClass('btn-borrar');
    });
    // Boton borrar y modal nota 2 
    $('#deleteModal2').on('show.bs.modal', function (e) {
        console.log("El modal borrar 2 comenzó a abrirse");
        $('#deleteBtn2').removeClass('btn-borrar');
        $('#deleteBtn2').addClass('btn-success');
        $('#deleteBtn2').prop('disabled', true);
    });
    $('#deleteModal2').on('shown.bs.modal', function (e) {
        console.log("El modal borrar 2 termino de abrirse");

    });
    $('#deleteModal2').on('hide.bs.modal', function (e) {
        console.log("El modal borrar 2 comenzó a ocultarse")
    });
    $('#deleteModal2').on('hidden.bs.modal', function (e) {
        console.log("El modal borrar 2 termino de cerrarse");
        $('#deleteBtn2').prop('disabled', false);
        $('#deleteBtn2').removeClass('btn-success');
        $('#deleteBtn2').addClass('btn-borrar');
    });
    // Boton borrar y modal nota 3 
    $('#deleteModal3').on('show.bs.modal', function (e) {
        console.log("El modal borrar 3 comenzó a abrirse");
        $('#deleteBtn3').removeClass('btn-borrar');
        $('#deleteBtn3').addClass('btn-success');
        $('#deleteBtn3').prop('disabled', true);
    });
    $('#deleteModal3').on('shown.bs.modal', function (e) {
        console.log("El modal borrar 3 termino de abrirse");

    });
    $('#deleteModal3').on('hide.bs.modal', function (e) {
        console.log("El modal borrar 3 comenzó a ocultarse")
    });
    $('#deleteModal3').on('hidden.bs.modal', function (e) {
        console.log("El modal borrar 3 termino de cerrarse");
        $('#deleteBtn3').prop('disabled', false);
        $('#deleteBtn3').removeClass('btn-success');
        $('#deleteBtn3').addClass('btn-borrar');
    });
});